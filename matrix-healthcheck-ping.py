"""
    matrix-healthcheck-ping
    ~~~~~~~~~~~~~~~~~~~~~~~

    `matrix-healthcheck-ping` is a simple matrix bot that continuously sends a
    message in a matrix room and expects a response. If it does not receive a
    response within a preset timeout, it will let its' operator know that
    something is wrong.

    It is created to help operate the Cactus Comments application service
    (https://gitlab.com/cactus-comments/cactus-appservice) reliably, but we've
    tried to make it generic enough so it can be used for other things as well.

    Copyright (C) 2021 Carl Bordum Hansen and Asbjørn Olling.

    Licensed under the GNU Affero General Public License v3.0.
"""


from dataclasses import dataclass
import itertools
import os
import sys
import time

import requests
from structlog import get_logger


log = get_logger()


@dataclass
class Settings:
    userid: str
    password: str
    homeserver_url: str
    target_matrix_id: str
    alert_matrix_id: str

    message: str = "help"
    alert_message_fmt: str = "Error! No response in {target_matrix_id} in time!"
    timeout_seconds: int = 60
    retry_seconds: int = 60 * 5
    retry_seconds_after_alert: int = 60 * 60  # 60s * 60m = 1h

    @property
    def alert_message(self):
        return self.alert_message_fmt.format(**self.__dict__)

    @classmethod
    def from_env(cls):
        """Create new `Settings` from environment variables."""

        log.msg("Reading configuration from environment")

        def _read_required_var(var):
            if not (result := os.getenv(var, False)):
                print(f"${var} is required, but was not set.", file=sys.stderr)
                sys.exit(1)
            return result

        userid = _read_required_var("HC_USERID") or _read_required_var("HC_USERNAME")
        if not (userid.startswith("@") and ":" in userid):
            log.error("Bad userid format.", bad_user_id=userid)
            sys.exit(1)

        password = _read_required_var("HC_PASSWORD")
        homeserver_url = _read_required_var("HC_HOMESERVER_URL")
        target_matrix_id = _read_required_var("HC_TARGET_MATRIX_ID")
        alert_matrix_id = _read_required_var("HC_ALERT_MATRIX_ID")

        message = os.getenv("HC_MESSAGE", cls.message)
        alert_message_fmt = os.getenv("HC_ALERT_MESSAGE_FMT", cls.alert_message_fmt)
        timeout_seconds = int(os.getenv("HC_TIMEOUT_SECONDS", cls.timeout_seconds))
        retry_seconds = int(os.getenv("HC_RETRY_SECONDS", cls.retry_seconds))
        retry_seconds_after_alert = int(
            os.getenv("HC_RETRY_SECONDS_AFTER_ALERT", cls.retry_seconds_after_alert)
        )

        return cls(
            userid,
            password,
            homeserver_url,
            target_matrix_id,
            alert_matrix_id,
            message,
            alert_message_fmt,
            timeout_seconds,
            retry_seconds,
            retry_seconds_after_alert,
        )


class MatrixHealthCheckClient:
    def __init__(self, settings):
        self.s = settings
        self.txn_id = itertools.count()
        self.next_batch = None  # used for syncing

    @classmethod
    def from_env(cls):
        settings = Settings.from_env()
        return cls(settings)

    @property
    def retry_seconds(self):
        return self.s.retry_seconds

    @property
    def retry_seconds_after_alert(self):
        return self.s.retry_seconds_after_alert

    @property
    def userid(self):
        return self.s.userid

    def login(self):
        log.msg(
            "Trying to log into homeserver",
            homeserver=self.s.homeserver_url,
            userid=self.s.userid,
        )
        r = requests.post(
            f"{self.s.homeserver_url}/_matrix/client/r0/login",
            json={
                "type": "m.login.password",
                "identifier": {
                    "type": "m.id.user",
                    "user": self.s.userid,
                },
                "password": self.s.password,
            },
        )
        r.raise_for_status()
        log.msg("Successfully logged into homeserver")
        self.access_token = r.json()["access_token"]

    @property
    def _headers(self):
        return {
            "Authorization": f"Bearer {self.access_token}",
        }

    def send_message(self):
        log.msg("Sending message!", target_matrix_id=self.s.target_matrix_id)
        requests.put(
            f"{self.s.homeserver_url}/_matrix/client/r0/rooms/{self.s.target_matrix_id}/send/m.room.message/{next(self.txn_id)}",
            json={
                "msgtype": "m.text",
                "body": self.s.message,
            },
            headers=self._headers,
        ).raise_for_status()

    def send_alert(self):
        log.warn("Sending alert!", alert_matrix_id=self.s.alert_matrix_id)
        requests.put(
            f"{self.s.homeserver_url}/_matrix/client/r0/rooms/{self.s.alert_matrix_id}/send/m.room.message/{next(self.txn_id)}",
            json={
                "msgtype": "m.text",
                "body": self.s.alert_message,
            },
            headers=self._headers,
        ).raise_for_status()

    def wait_for_answer(self):
        r = requests.get(
            f"{self.s.homeserver_url}/_matrix/client/r0/sync",
            params={
                "timeout": self.s.timeout_seconds,
                "since": self.next_batch,
            },
            headers=self._headers,
        )
        r.raise_for_status()
        j = r.json()
        self.next_batch = j["next_batch"]
        return j["rooms"]["join"][self.s.target_matrix_id]["timeline"]["events"]


def main():
    log.msg("Starting matrix-healthcheck-ping")
    client = MatrixHealthCheckClient.from_env()

    client.login()

    while True:
        client.send_message()

        events = client.wait_for_answer()
        if any(filter(lambda e: e["sender"] != client.userid, events)):
            log.msg("Got response")
            time.sleep(client.retry_seconds)
        else:
            client.send_alert()
            time.sleep(client.retry_seconds_after_alert)


if __name__ == "__main__":
    main()
