# matrix-healthcheck-ping

`matrix-healthcheck-ping` is a simple matrix bot that continuously sends a
message in a matrix room and expects a response. If it does not receive a
response within a preset timeout, it will let its' operator know that something
is wrong.

It is created to help operate the [Cactus Comments application
service](https://gitlab.com/cactus-comments/cactus-appservice) reliably, but
we've tried to make it generic enough so it can be used for other things as
well.

`matrix-healthcheck-ping` is alpha software.

`matrix-healthcheck-ping` is available on [Docker
Hub](https://hub.docker.com/r/cactuscomments/matrix-healthcheck-ping).


## License

Copyright (C) 2021 Carl Bordum Hansen and Asbjørn Olling

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
