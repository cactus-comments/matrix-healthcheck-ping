FROM python:3.9-alpine3.14

# Force the stdout and stderr streams from python to be unbuffered.
ENV PYTHONUNBUFFERED=1

RUN pip install --no-cache-dir requests==2.26.0 structlog==21.1.0 colorama==0.4.4

WORKDIR /code

COPY matrix-healthcheck-ping.py .

CMD ["python", "matrix-healthcheck-ping.py"]
